db.users.insertMany([
	
	{
		"firstName": "Gawr",
		"lastName": "Gura",
		"email": "aaaaaaaaa@gmail.com",
		"password": "iAmshark",
		"isAdmin": false
	},
	{
		"firstName": "Mori",
		"lastName": "Calliope",
		"email": "shinigamidesu@gmailcom",
		"password": "reaperrapper",
		"isAdmin": false
	},
	{
		"firstName": "Kiara",
		"lastName": "Takanashi",
		"email": "aggressivelyphoenix@gmail.com",
		"password": "notachicken",
		"isAdmin": false
	},
	{
		"firstName": "Amelia",
		"lastName": "Watson",
		"email": "bestDetective@gmail.com",
		"password": "timetravelgremlin",
		"isAdmin": false
	},
	{
		"firstName": "Ina'nis",
		"lastName": "Ninomae",
		"email": "ancientone@gmail.com",
		"password": "worldDomination",
		"isAdmin": false
	}
])

db.courses.insertMany([
	{
		"name": "Biology 101",
		"price": 1000,
		"isActive": false
	},
	{
		"name": "Biology 201",
		"price": 2000,
		"isActive": false
	},
	{
		"name": "Biology 202",
		"price": 2500,
		"isActive": false
	}
])

db.users.find({"isAdmin": false})

db.users.updateOne({}, {$set: {"isAdmin": true}})

db.courses.updateOne({"price": 2000}, {$set: {"isActive": true}})

db.courses.deleteMany({"isActive": false})
